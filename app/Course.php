<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';

    protected $fillable = [
        'user_id', 'title', 'thumbnail', 'description', 'created_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\User;
use App\Http\Resources\CourseResource;
use App\Http\Resources\CourseResourceCollection;

class CourseController extends Controller
{
    public function index()
    {
        return new CourseResourceCollection(Course::paginate());
    }

    public function show(Course $course): CourseResource
    {
        $c = new CourseResource($course);
        return $c;
    }

    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'title'   => 'required',
            'description' => 'required',
        ]);
        $course = Course::create($request->all());
        return new CourseResource($course);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Module;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Module::create([
            'course_id' => 1,
            'title' => 'Materi 1',
            'content' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
                            Impedit, consequuntur veniam? Praesentium voluptatem molestiae aliquam quos non id cum numquam quidem quo ipsa, 
                            a ex blanditiis voluptate repellat laboriosam qui.',
        ]);
        Module::create([
            'course_id' => 1,
            'title' => 'Materi 2',
            'content' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
                            Impedit, consequuntur veniam? Praesentium voluptatem molestiae aliquam quos non id cum numquam quidem quo ipsa, 
                            a ex blanditiis voluptate repellat laboriosam qui.',
        ]);Module::create([
            'course_id' => 1,
            'title' => 'Materi 3',
            'content' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
                            Impedit, consequuntur veniam? Praesentium voluptatem molestiae aliquam quos non id cum numquam quidem quo ipsa, 
                            a ex blanditiis voluptate repellat laboriosam qui.',
        ]);
        Module::create([
            'course_id' => 2,
            'title' => 'Materi 1',
            'content' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
                            Impedit, consequuntur veniam? Praesentium voluptatem molestiae aliquam quos non id cum numquam quidem quo ipsa, 
                            a ex blanditiis voluptate repellat laboriosam qui.',
        ]);
        Module::create([
            'course_id' => 2,
            'title' => 'Materi 2',
            'content' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
                            Impedit, consequuntur veniam? Praesentium voluptatem molestiae aliquam quos non id cum numquam quidem quo ipsa, 
                            a ex blanditiis voluptate repellat laboriosam qui.',
        ]);Module::create([
            'course_id' => 3,
            'title' => 'Materi 1',
            'content' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
                            Impedit, consequuntur veniam? Praesentium voluptatem molestiae aliquam quos non id cum numquam quidem quo ipsa, 
                            a ex blanditiis voluptate repellat laboriosam qui.',
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Exam;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Exam::create([
            'course_id' => 1,
        ]);
        Exam::create([
            'course_id' => 2,
        ]);
        Exam::create([
            'course_id' => 3,
        ]);
    }
}

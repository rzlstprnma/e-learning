<?php

use Illuminate\Database\Seeder;
use App\Question;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Question::create([
            'exam_id' => 1,
            'question' => 'First Question'
        ]);
        Question::create([
            'exam_id' => 1,
            'question' => 'Second Question'
        ]);
        Question::create([
            'exam_id' => 1,
            'question' => 'Third Question'
        ]);
        Question::create([
            'exam_id' => 2,
            'question' => 'First Question'
        ]);
        Question::create([
            'exam_id' => 2,
            'question' => 'Second Question'
        ]);
        Question::create([
            'exam_id' => 3,
            'question' => 'First Question'
        ]);
    }
}
